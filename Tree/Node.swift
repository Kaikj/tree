/**
 `Node` is the internal representation of a vertex in a tree. It is a generic
 type with a type parameter `T` that is the type of the node's label. For
 example, `Node<String>` is the type of node with a String label and 
 `Node<Int>` is the type of node with a Int label

 Additionally, due to well defined properties of a tree, each node in the tree
 stores representations of it's parent node as well as it's children nodes.
 This not only simplifies subsequent operations but makes it unneccessary to
 define a TreeEdge struct to store this information

 Because we need to compare the node label, the type parameter `T` needs to
 conform to `Equatable` protocol. Since it is easier to ensure uniqueness in
 a Set, we make `TreeNode` conform to Hashable.
 */

public class Node<T: Hashable>: Hashable {
    public var label: T

    // The node can have no parent if it is the root
    public var parent: Node<T>?

    // An array because order is important
    public var children = [Node<T>]()

    // MARK: Hashable
    public var hashValue: Int {
        return label.hashValue
    }

    public init(_ label: T) {
        self.label = label
    }

    /// Add the provided node as a child
    public func addChild(child: Node<T>) {
        // Do nothing if the child is already added
        guard !children.contains(child) else {
            return
        }

        // Do nothing if it is the same node
        guard child != self else {
            return
        }

        children.append(child)
        child.addParent(self)
    }

    /// Add the provided node as a parent
    public func addParent(parent: Node<T>) {
        // Do nothing if there is already a parent
        guard self.parent == nil else {
            return
        }

        // Do nothing if it is the same node
        guard parent != self else {
            return
        }

        self.parent = parent
        parent.addChild(self)
    }

    /// Get the current depth of this node
    /// relative to the root
    public func getDepth() -> Int {
        var depth = 0
        var currentNode = self
        while let parent = currentNode.parent {
            currentNode = parent
            depth++
        }
        return depth
    }

    /// Allows for checking if it is a root
    public func isRoot() -> Bool {
        return self.parent == nil
    }
}

// MARK: Equatable
public func ==<T>(lhs: Node<T>, rhs: Node<T>) -> Bool {
    return lhs.label == rhs.label
}