/**
 `Tree` represents a tree data structure. It is a collection of nodes
 starting from a root node. A tree as a data structure is a type of graph
 with additional constraints:
    - There are no cycles in the tree
    - All nodes are connected

 Additionally, there are more specific terms to be defined:
    - A node directly connected to another node when moving away
        from the root is known as the child
    - A node directly connected to another node when moving towards
        the root is known as the parent
    - A node that does not have any parents is the root node

 The representation invariants for every Tree t:
    - There is only one root
    - No 2 nodes in t can share the same child
    - There are no cycles in t
    - Each node has a unique label
 
 `Tree` is a generic type with a type parameter `T` that defines the type of
 nodes' labels.
*/

public class Tree<T: Hashable> {
    public typealias N = Node<T>

    public init() {}
    public var root: N? = nil
    public var count: Int {
        return nodes.count
    }

    /// Computes a breadth first array of nodes
    public var nodes: [N] {
        var visited = Set<N>()
        var toVisit = Queue<N>()
        var order = [N]()

        // Empty tree, no order returned
        guard let root = root else {
            return order
        }

        toVisit.enqueue(root)

        while let node = try? toVisit.dequeue() {
            // Do not revisit a node
            guard !visited.contains(node) else {
                continue
            }

            // Mark node as visited
            visited.insert(node)
            order.append(node)

            // Add all unvisited children into the queue
            // Already in the correct order, the first element
            // in the adjacency list will be dequeued first
            node.children
                .filter() {child in !visited.contains(child)}
                .forEach() {child in toVisit.enqueue(child)}
        }
        
        return order
    }

    /// Check if tree is an empty tree.
    /// - Returns: true if the tree is empty.
    public func isEmpty() -> Bool {
        return root == nil
    }

    /// Check if tree contains a node with the label provided.
    /// - Returns: true if the tree contains the label.
    public func contains(label: T) -> Bool {
        let node = N(label)
        return nodes.contains(node)
    }

    /// Finds the node that has the label provided.
    /// - Returns: The node that has the label. Nil
    ///     will be returned if there is no node with
    ///     the label provided
    public func find(label: T) -> N? {
        guard let index = nodes.indexOf(N(label)) else {
            return nil
        }

        return nodes[index]
    }

    /// Get the parent of the node with the label provided.
    /// - Returns: the label of the parent if it exists. Nil 
    ///     will be returned if there is no node with the
    ///     label provided
    public func getParent(label: T) -> N? {
        // Label provided does not exist as a node
        guard let node = find(label) else {
            return nil
        }

        // Node has no parent
        guard let parent = node.parent else {
            return nil
        }

        return parent
    }

    /// Get an array of the children of the node with the
    /// label provided.
    /// - Returns: the array of labels of the children. An
    ///     empty array will be returned if there are no
    ///     chidren existing. Nil will be returned if there
    ///     is no node with the label provided
    public func getChildren(label: T) -> [N]? {
        // Label provided does not exist as a node
        guard let node = find(label) else {
            return nil
        }

        return node.children
    }

    /// Add the provided label to the tree as a node.
    /// If the node already exists in the tree, do nothing.
    /// The node must either be the root or connected to an
    /// existing node. If no parent is provided, assume that
    /// it is connected to the root.
    public func addNode(label: T, parent: T? = nil) -> N {
        // Check to make sure that label does not already exist
        guard !contains(label) else {
            return find(label)!
        }

        let node = N(label)

        if isEmpty() && parent == nil {
            // There are no nodes in the tree so this is just
            // the root node
            root = node
        } else if isEmpty() {
            // The parent node of the addedNode is not added
            // into the tree yet. At this point, we know that
            // parent is not nil.
            root = N(parent!)
            root!.addChild(node)
        } else if parent == nil {
            // The addedNode is added to the root. At this
            // point, we know that there is a root
            root!.addChild(node)
        } else {
            // The addedNode is added to it's parent
            let parentNode = N(parent!)
            parentNode.addChild(node)
        }
        return node
    }

    /// Counts the number of nodes connected to the root node.
    private func countNodes(root: N?) -> Int {
        guard root != nil else {
            return 0
        }

        return 1 + root!.children.reduce(0) {
            sum, child in sum + countNodes(child)
        }
    }
}
