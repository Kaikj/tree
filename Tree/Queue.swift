//
//  Queue.swift
//  DataStructures
//
//  Copyright (c) 2016 NUS CS3217. All rights reserved.
//

enum QueueError: ErrorType {
    case EmptyQueue
}

struct Queue<T> {

    // The variable that contains the queue
    private var queue = [T]()

    /**
     Adds an element to the tail of the queue.

     - Parameter item: The element to be added to the queue
     */
    mutating func enqueue(item: T) {
        // Our queue implementation enqueues new items
        // to the back of the underlying array
        queue.append(item)
    }

    /**
     Removes an element from the head of the queue and return it.

     - Returns: item at the head of the queue

     - Throws: QueueError.EmptyQueue
     */
    mutating func dequeue() throws -> T {
        guard !queue.isEmpty else {
            throw QueueError.EmptyQueue
        }

        // Our queue implementation dequeues items
        // from the front of the underlying array
        return queue.removeFirst()
    }

    /**
     Returns, but does not remove, the element at the head of the queue.

     - Returns: item at the head of the queue

     - Throws: QueueError.EmptyQueue
     */
    func peek() throws -> T {
        guard !queue.isEmpty else {
            throw QueueError.EmptyQueue
        }

        // Since the front of the queue is at the front
        // of the array, we can just return the first element
        return queue.first!
    }

    /**
     - Returns: number of element in the queue
     */
    var count: Int {
        return queue.count
    }

    /**
     - Returns: true if the queue is empty and false otherwise
     */
    var isEmpty: Bool {
        return queue.isEmpty
    }

    /**
     Removes all elements in the queue.
     */
    mutating func removeAll() {
        queue.removeAll()
    }

    /**
     - Returns: array of element in their respective dequeue order. i.e. first element in the array
     is the first element to be dequeued
     */
    func toArray() -> [T] {
        // Due to the implementation choice of appending new
        // items, the underlying array is already in their
        // respective dequeue order, making this trivial
        return queue
    }
}