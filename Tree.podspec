Pod::Spec.new do |s|
  s.name         = "Tree"
  s.version      = "0.0.12"
  s.summary      = "A simple implementation of a tree data structure"
  s.homepage     = "https://bitbucket.org/Kaikj/tree"
  s.license      = "MIT"
  s.author       = { "Kaikj" => "bluesword246@gmail.com" }
  s.platform     = :ios
  s.source       = { :git => "https://bitbucket.org/Kaikj/tree.git" }
  s.source_files = "Tree"
end
